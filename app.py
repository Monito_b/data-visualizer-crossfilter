import dash
import math
from dash.dependencies import State, Input, Output
import dash_core_components as dcc
import dash_html_components as html
import plotly.graph_objs as go
import pandas as pd
import numpy as np

import plotly.graph_objs as go
import numpy as np

app = dash.Dash()

N = 500

df = pd.read_csv('https://gist.githubusercontent.com/Monitob/737de59dae128d4c38b8103740ecc3c5/raw/66fdc7d9f9683ed5bc987f63d4113db0778c1250/FondsRisk')

# Get a list of unique years in the dataframe
portfolios = sorted(list(df.id_ptf.unique()))
available_managers = df['manager'].unique()

df_conservative = df.query('profile == ["Conservative"]')
df_defensive = df.query('profile == ["Defensive"]')
df_balanced = df.query('profile == ["Balanced"]')
df_dynamic = df.query('profile == ["Dynamic"]')
df_agressive = df.query('profile == ["Aggressive"]')

xaxis_column_name = "Performance"
yaxis_column_name = "Value at Risk"

Defensive_data = go.Scatter(

    x = df_defensive['Perf_value'],
    y = df_defensive['Indicator_value'],
    text = df_defensive['ptf_name'],
    name = 'Defensive',
    mode = 'markers',
    showlegend=True,
    # opacity = 0.7,
    marker = dict(
        size = 15,
        color = 'rgba(132, 214, 0, .9)',
        line = dict(
            width = 0.5,
            color = 'white'
        )
    )
)

Conservative_data = go.Scatter(

    x = df_conservative['Perf_value'],
    y = df_conservative['Indicator_value'],
    text = df_conservative['ptf_name'],
    name = 'Conservative',
    mode = 'markers',
        showlegend=True,
    # opacity = 0.7,
    marker = dict(
        size = 15,
        color = 'rgba(0, 137, 0, .9)',
        line = dict(
            width = 0.5,
            color = 'white'
        )
    )
)

Balanced_data = go.Scatter(

    x = df_balanced['Perf_value'],
    y = df_balanced['Indicator_value'],
    text = df_balanced['ptf_name'],
    name = 'Balanced',
    mode = 'markers',
    showlegend=True,
    marker = dict(
        size = 15,
        color = 'rgba(249, 255, 0, .9)',
        line = dict(
            width = 0.5,
            color = 'white'
        )
    )
)

Dynamic_data = go.Scatter(

    x = df_dynamic['Perf_value'],
    y = df_dynamic['Indicator_value'],
    text = df_dynamic['ptf_name'],
    name = 'Dynamic',
    mode = 'markers',
    showlegend=True,
    marker = dict(
        size = 15,
        color = 'rgba(255, 206, 0, .9)',
        line = dict(
            width = 0.5,
            color = 'white'
        )
    )
)

Aggressive_data = go.Scatter(

    x = df_agressive['Perf_value'],
    y = df_agressive['Indicator_value'],
    text = df_agressive['ptf_name'],
    name = 'Aggressive',
    mode = 'markers',
    showlegend=True,
    marker = dict(
        size = 15,
        color = 'rgba(255, 0, 0, .9)',
        line = dict(
            width = 0.5,
            color = 'white'
        )
    )
)

# Risk \ Return dashboard
app.layout = html.Div([

    html.Div([
        html.Label('Manager: '),
        dcc.Dropdown(
            id='filter-manager',
            options=[{'label': i, 'value': i} for i in available_managers],
            multi=True
        ),
         html.Div([
        dcc.Graph(
            id='risk-vs-var'
        )
    ]),
        ],
    style={'width': '60%', 'display': 'inline-block'})
], style={'width': '1500'})

def createScatter(df_current, profile, manager):

    color = ''    
    if profile == 'Conservative':
        color = 'rgba(0, 137, 0, .9)'
    elif profile == 'Defensive':
        color = 'rgba(132, 214, 0, .9)'
    elif profile == 'Balanced':
        color = 'rgba(249, 255, 0, .9)'
    elif profile == 'Dynamic':
        color = 'rgba(255, 206, 0, .9)'
    elif profile == 'Aggressive':
        color = 'rgba(255, 0, 0, .9)'



    return go.Scatter(

    x = df_current['Perf_value'],
    y = df_current['Indicator_value'],
    text = df_current['ptf_name'],
    name = profile + " " + manager,
    mode = 'markers',
        showlegend=True,
    # opacity = 0.7,
    marker = dict(
        size = 15,
        color = color,
        line = dict(
            width = 0.5,
            color = 'white'
        )
    )
)
def filter_manager(manager):
    df_co = df_conservative.where(df.manager == manager)
    df_de = df_defensive.where(df.manager == manager)
    df_ba = df_balanced.where(df.manager == manager)
    df_dy = df_dynamic.where(df.manager == manager)
    df_ag = df_agressive.where(df.manager == manager)
    return [createScatter(df_co, "Conservative", manager),
            createScatter(df_de, "Defensive", manager),
            createScatter(df_ba, "Balanced", manager),
            createScatter(df_dy, "Dynamic", manager),
            createScatter(df_ag, "Aggressive", manager)
            ]

def defaultGraph():
    data_df = [Defensive_data, Conservative_data, Balanced_data, Aggressive_data, Dynamic_data]
    return {
            'data': data_df,
            'layout': go.Layout(
                xaxis={
                    'title': xaxis_column_name
                },
                yaxis={
                    'title': yaxis_column_name
                },
                margin={'l': 40, 'b': 30, 't': 10, 'r': 0},
                height=450,
                hovermode='closest'
            )
    }

@app.callback(
    Output('risk-vs-var', 'figure'),
    [Input('filter-manager', 'value')])
def update_graph(selected_dropdown_value):
    print(selected_dropdown_value)
    if selected_dropdown_value == None or len(selected_dropdown_value) == 0:
        return defaultGraph()
    if selected_dropdown_value is not None:
        data_filter = []
        for i in selected_dropdown_value:
            temparray = filter_manager(i)
            for j in temparray:
                data_filter.append(j)

        print(data_filter)
        return {
            'data': data_filter,
            'layout': go.Layout(
                xaxis={
                    'title': xaxis_column_name
                },
                yaxis={
                    'title': yaxis_column_name
                },
                margin={'l': 40, 'b': 30, 't': 10, 'r': 0},
                height=450,
                hovermode='closest'
         )}

if __name__ == '__main__':
    app.run_server(debug=True, port=9000)